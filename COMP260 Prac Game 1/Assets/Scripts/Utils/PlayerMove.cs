﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	
	public Vector2 velocity; // in metres per second

	public float maxSpeed = 5.0f;

	void Update() {
		
		if(this.gameObject.tag == "Player") {
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");
			Vector2 velocity = direction * maxSpeed;
			transform.Translate (velocity * Time.deltaTime);
		}

		if(this.gameObject.tag == "Player2") {
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal1");
			direction.y = Input.GetAxis ("Vertical1");
			Vector2 velocity = direction * maxSpeed;
			transform.Translate (velocity * Time.deltaTime);
		}
	}
}