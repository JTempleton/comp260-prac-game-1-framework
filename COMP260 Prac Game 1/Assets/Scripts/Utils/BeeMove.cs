﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {
	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	public Transform target2;
	private Vector2 heading = Vector2.right; 

	void Update() {

		var distance = Vector2.Distance (transform.position,target.position);
		var distance2 = Vector2.Distance (transform.position,target2.position);

		if(distance < distance2) {
			Vector2 direction = target.position - transform.position;
			float angle = turnSpeed * Time.deltaTime;
				if (direction.IsOnLeft(heading)) {
					heading = heading.Rotate(angle);
				}
				else {
					heading = heading.Rotate(-angle);
				}
			transform.Translate(heading * speed * Time.deltaTime);
		}
		else {
			Vector2 direction = target2.position - transform.position;	
			float angle = turnSpeed * Time.deltaTime;
				if (direction.IsOnLeft(heading)) {
					heading = heading.Rotate(angle);
				}
				else {
					heading = heading.Rotate(-angle);
				}
			transform.Translate(heading * speed * Time.deltaTime);
		}
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}
}
